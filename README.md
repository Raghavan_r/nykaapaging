# NykaaPaging



## Technologies Used

- MVVM + Clean Architecture
- Kotlin
- Hilt
- Coroutines
- Flow
- Lifecycle Extension
- Paging Library 3
- Retrofit
- Coil
- Constraintlayout
- Material Design


## Features

- Home screen which loads Data using recyclerview
- Handled of back to top option
- Handled screen orientation changes
- Used coroutines to hit api call
- Used paging library 3 to load items by 20
- Used coil to load the images
- Used flow as observer
- Used retrofit to hit the api


### Notes

- The Mock api for the 4th page is throwing Malformat Json, since one curly braces ({) at the end of the response was more. Since after the 4th page it will show something went wrong. Once the API team fixed it then will show No more products found text without any changes from app need.
