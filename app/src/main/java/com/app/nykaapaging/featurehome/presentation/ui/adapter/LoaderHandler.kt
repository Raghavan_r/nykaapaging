package com.app.nykaapaging.featurehome.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.nykaapaging.databinding.LoaderRowItemBinding


class LoaderHandler :
    LoadStateAdapter<LoaderHandler.LoadStateViewHolder>() {

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = LoaderRowItemBinding.inflate(layoutInflater, parent, false)
        return LoadStateViewHolder(
            itemBinding
        )
    }

    inner class LoadStateViewHolder(private val binding: LoaderRowItemBinding) :
        RecyclerView.ViewHolder(binding.root){
            fun bind(loadState: LoadState) {

                if (loadState is LoadState.Loading) {
                    binding.progressBar.isVisible = true
                    binding.tvLoadResult.isVisible = false

                } else {
                    binding.tvLoadResult.isVisible = false
                    binding.progressBar.isGone = true
                }

                if (loadState is LoadState.Error) {
                    binding.progressBar.isVisible = false
                    binding.tvLoadResult.isVisible = true
                    binding.tvLoadResult.text = loadState.error.message
                }
            }
        }
}