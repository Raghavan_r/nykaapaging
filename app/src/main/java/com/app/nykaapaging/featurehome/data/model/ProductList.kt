package com.app.nykaapaging.featurehome.data.model


import com.google.gson.annotations.SerializedName

data class ProductList(
    @SerializedName("message")
    val message: String?,
    @SerializedName("response")
    val response: Response?,
    @SerializedName("status")
    val status: String?,
    @SerializedName("type")
    val type: String?
) {
    data class Response(
        @SerializedName("android_landing_url")
        val androidLandingUrl: Any?,
        @SerializedName("app_sorting")
        val appSorting: String?,
        @SerializedName("art_content")
        val artContent: Any?,
        @SerializedName("art_link_text")
        val artLinkText: Any?,
        @SerializedName("art_pos")
        val artPos: Any?,
        @SerializedName("art_title")
        val artTitle: Any?,
        @SerializedName("art_url")
        val artUrl: Any?,
        @SerializedName("auth_certificate")
        val authCertificate: Any?,
        @SerializedName("banner_image")
        val bannerImage: Any?,
        @SerializedName("banner_video")
        val bannerVideo: Any?,
        @SerializedName("banner_video_image")
        val bannerVideoImage: Any?,
        @SerializedName("category_name")
        val categoryName: String?,
        @SerializedName("child_categories")
        val childCategories: Any?,
        @SerializedName("desktop_tip_tile")
        val desktopTipTile: Any?,
        @SerializedName("filter_keys")
        val filterKeys: FilterKeys?,
        @SerializedName("filters")
        val filters: Filters?,
        @SerializedName("font_color")
        val fontColor: Any?,
        @SerializedName("ios_landing_url")
        val iosLandingUrl: Any?,
        @SerializedName("isNewApi")
        val isNewApi: Boolean?,
        @SerializedName("level")
        val level: Int?,
        @SerializedName("namespace")
        val namespace: List<String?>?,
        @SerializedName("offset")
        val offset: Int?,
        @SerializedName("other_filters")
        val otherFilters: OtherFilters?,
        @SerializedName("product_count")
        val productCount: Int?,
        @SerializedName("products")
        val products: List<Product>?,
        @SerializedName("query")
        val query: String?,
        @SerializedName("size_chart_image")
        val sizeChartImage: Any?,
        @SerializedName("sort")
        val sort: String?,
        @SerializedName("stop_further_call")
        val stopFurtherCall: Int?,
        @SerializedName("tip_tile")
        val tipTile: Any?,
        @SerializedName("title")
        val title: String?,
        @SerializedName("total_found")
        val totalFound: Int?,
        @SerializedName("type")
        val type: String?,
        @SerializedName("url")
        val url: String?
    ) {
        data class FilterKeys(
            @SerializedName("filters")
            val filters: List<Filter?>?,
            @SerializedName("other_filters")
            val otherFilters: List<OtherFilter?>?
        ) {
            data class Filter(
                @SerializedName("key")
                val key: String?,
                @SerializedName("title")
                val title: String?,
                @SerializedName("tracking_group")
                val trackingGroup: String?,
                @SerializedName("type")
                val type: String?
            )

            data class OtherFilter(
                @SerializedName("key")
                val key: String?,
                @SerializedName("title")
                val title: String?,
                @SerializedName("tracking_group")
                val trackingGroup: String?,
                @SerializedName("type")
                val type: String?
            )
        }

        data class Filters(
            @SerializedName("brand")
            val brand: List<Brand?>?,
            @SerializedName("category")
            val category: List<Category?>?,
            @SerializedName("discount")
            val discount: List<Discount?>?,
            @SerializedName("price")
            val price: Price?,
            @SerializedName("star_rating")
            val starRating: List<StarRating?>?
        ) {
            data class Brand(
                @SerializedName("count")
                val count: String?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("name")
                val name: String?
            )

            data class Category(
                @SerializedName("category_id")
                val categoryId: String?,
                @SerializedName("count")
                val count: String?,
                @SerializedName("depth")
                val depth: String?,
                @SerializedName("include_in_menu")
                val includeInMenu: String?,
                @SerializedName("lft")
                val lft: String?,
                @SerializedName("name")
                val name: String?,
                @SerializedName("parent_id")
                val parentId: String?,
                @SerializedName("position")
                val position: String?,
                @SerializedName("rgt")
                val rgt: String?
            )

            data class Discount(
                @SerializedName("count")
                val count: String?,
                @SerializedName("name")
                val name: String?,
                @SerializedName("value")
                val value: String?
            )

            data class Price(
                @SerializedName("max_price")
                val maxPrice: Double?,
                @SerializedName("min_price")
                val minPrice: Int?,
                @SerializedName("price_range")
                val priceRange: List<PriceRange?>?
            ) {
                data class PriceRange(
                    @SerializedName("count")
                    val count: String?,
                    @SerializedName("max")
                    val max: String?,
                    @SerializedName("min")
                    val min: String?
                )
            }

            data class StarRating(
                @SerializedName("count")
                val count: String?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("name")
                val name: String?
            )
        }

        data class OtherFilters(
            @SerializedName("top_brand")
            val topBrand: List<TopBrand?>?,
            @SerializedName("top_old_brand")
            val topOldBrand: List<TopOldBrand?>?
        ) {
            data class TopBrand(
                @SerializedName("count")
                val count: String?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("name")
                val name: String?
            )

            data class TopOldBrand(
                @SerializedName("count")
                val count: String?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("name")
                val name: String?
            )
        }

        data class Product(
            @SerializedName("brand_ids")
            val brandIds: String?,
            @SerializedName("brand_name")
            val brandName: String?,
            @SerializedName("button_text")
            val buttonText: String?,
            @SerializedName("catalog_tag")
            val catalogTag: List<String?>?,
            @SerializedName("category_ids")
            val categoryIds: String?,
            @SerializedName("discount")
            val discount: Int?,
            @SerializedName("dynamic_tags")
            val dynamicTags: List<Any?>?,
            @SerializedName("expdt")
            val expdt: Any?,
            @SerializedName("explore_more")
            val exploreMore: Any?,
            @SerializedName("fbn")
            val fbn: Int?,
            @SerializedName("final_price")
            val finalPrice: Int?,
            @SerializedName("from_gludo")
            val fromGludo: Int?,
            @SerializedName("gludo_stock")
            val gludoStock: Boolean?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("image_url")
            val imageUrl: String?,
            @SerializedName("is_backorder")
            val isBackorder: Int?,
            @SerializedName("is_kit_combo")
            val isKitCombo: Any?,
            @SerializedName("is_lux")
            val isLux: Int?,
            @SerializedName("is_saleable")
            val isSaleable: Boolean?,
            @SerializedName("mrp_freeze")
            val mrpFreeze: Any?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("object_type")
            val objectType: String?,
            @SerializedName("offers")
            val offers: Any?,
            @SerializedName("pack_size")
            val packSize: Any?,
            @SerializedName("price")
            val price: Int?,
            @SerializedName("primary_categories")
            val primaryCategories: PrimaryCategories?,
            @SerializedName("pro_flag")
            val proFlag: Int?,
            @SerializedName("quantity")
            val quantity: Int?,
            @SerializedName("rating")
            val rating: Double?,
            @SerializedName("rating_count")
            val ratingCount: Int?,
            @SerializedName("show_wishlist_button")
            val showWishlistButton: Int?,
            @SerializedName("sku")
            val sku: String?,
            @SerializedName("slug")
            val slug: String?,
            @SerializedName("type")
            val type: String?
        ) {
            data class PrimaryCategories(
                @SerializedName("l1")
                val l1: L1?,
                @SerializedName("l2")
                val l2: L2?,
                @SerializedName("l3")
                val l3: L3?
            ) {
                data class L1(
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("name")
                    val name: String?
                )

                data class L2(
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("name")
                    val name: String?
                )

                data class L3(
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("name")
                    val name: String?
                )
            }
        }
    }
}
