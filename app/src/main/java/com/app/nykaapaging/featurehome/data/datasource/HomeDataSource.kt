package com.app.nykaapaging.featurehome.data.datasource

import com.app.nykaapaging.featurehome.data.model.ProductList
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeDataSource {

    @GET("products")
    suspend fun  getProductList(@Query("page") pageNo: Int): ProductList
}