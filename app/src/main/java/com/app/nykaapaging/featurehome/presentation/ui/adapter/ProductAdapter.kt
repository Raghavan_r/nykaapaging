package com.app.nykaapaging.featurehome.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.app.nykaapaging.databinding.ProductsRowItemBinding
import com.app.nykaapaging.featurehome.data.model.ProductList.Response.Product

class ProductAdapter : PagingDataAdapter<Product, ProductAdapter.DataViewHolder>(REPO_COMPARATOR) {

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(
                oldItem: Product,
                newItem: Product
            ): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: Product,
                newItem: Product
            ): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ProductsRowItemBinding.inflate(layoutInflater, parent, false)
        return DataViewHolder(
            itemBinding
        )
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val item = getItem(position)
        if (item != null) {
            holder.bind(item)
        }
    }

    inner class DataViewHolder(
        private val itemBinding: ProductsRowItemBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        @MainThread
        fun bind(data: Product) {
            itemBinding.tvDesc.text = data.name
            itemBinding.tvAmount.text = "₹"+data.finalPrice
            itemBinding.rbProduct.stepSize = 0.01f
            itemBinding.rbProduct.rating = data.rating?.toFloat() ?: 0f
            itemBinding.tvReviewCount.text = "("+data.ratingCount+")"
            itemBinding.ivProduct.load(data.imageUrl)
        }
    }
}