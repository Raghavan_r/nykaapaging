package com.app.nykaapaging.featurehome.domain.usecase

import com.app.nykaapaging.featurehome.domain.repo.HomeRepo
import javax.inject.Inject

class HomeUseCase @Inject constructor(private val homeRepo: HomeRepo) {

    suspend fun getProductList() = homeRepo.getProductList()
}