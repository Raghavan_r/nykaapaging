package com.app.nykaapaging.featurehome.data.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.app.nykaapaging.featurehome.data.model.ProductList
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class HomePagingSource(private val dataSource: HomeDataSource) : PagingSource<Int, ProductList.Response.Product>() {
    override fun getRefreshKey(state: PagingState<Int, ProductList.Response.Product>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductList.Response.Product> {
        val page = params.key ?: STARTING_PAGE_INDEX
        return try {


            val data = dataSource.getProductList(page)
            if(data.response?.products?.isEmpty() == true){
                LoadResult.Error(
                    IOException("No more products found")
                )
            }else{
                LoadResult.Page(
                    data = data.response?.products.orEmpty(),
                    prevKey = null,
                    nextKey = page + 1
                )
            }


        } catch (jsonException : Throwable){
            val error = IOException("Something went wrong")
            LoadResult.Error(error)
        }
        catch (exception: IOException) {
            val error = IOException("Please Check Internet Connection")
            LoadResult.Error(error)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }
}