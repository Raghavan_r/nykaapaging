package com.app.nykaapaging.featurehome.presentation.ui.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.nykaapaging.core.base.BaseFragment
import com.app.nykaapaging.core.utils.collectLatestLifecycleFlow
import com.app.nykaapaging.databinding.FragmentHomeBinding
import com.app.nykaapaging.featurehome.presentation.ui.adapter.LoaderHandler
import com.app.nykaapaging.featurehome.presentation.ui.adapter.ProductAdapter
import com.app.nykaapaging.featurehome.presentation.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding
    private val viewModel: HomeViewModel by viewModels()

    private val adapter = ProductAdapter ()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(
            inflater,
            container,
            false
        )
        setAdapter()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        collectLatestLifecycleFlow(viewModel.productData, viewLifecycleOwner) {
            it?.let { it1 ->
                adapter.submitData(it1)
            }
        }

    }

    private fun setAdapter() {

        val layoutManager = GridLayoutManager(context, 2)
        layoutManager.spanSizeLookup =  object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == adapter.itemCount  && adapter.itemCount > 0) {
                    2
                } else {
                    1
                }
            }
        }
        binding.rvProduct.layoutManager = layoutManager
        binding.rvProduct.adapter = adapter.withLoadStateFooter(
            footer = LoaderHandler()
        )
    }

    override fun setOnclickListener() {
        binding.backToTop.setOnClickListener {
            binding.rvProduct.smoothScrollToPosition(0)
        }

        binding.rvProduct.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    Handler(Looper.getMainLooper()).postDelayed(
                         { binding.backToTop.isVisible = false },
                        2000
                    )
                } else if (dy < 0) {
                    binding.backToTop.isVisible = true
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Handler(Looper.getMainLooper()).postDelayed(
                         { binding.backToTop.isVisible = false },
                        2000
                    )
                }
            }
        })
    }
}