package com.app.nykaapaging.featurehome.data.repo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.app.nykaapaging.featurehome.data.datasource.HomeDataSource
import com.app.nykaapaging.featurehome.data.datasource.HomePagingSource
import com.app.nykaapaging.featurehome.domain.repo.HomeRepo
import javax.inject.Inject

class HomeRepoImpl @Inject constructor(private val dataSource: HomeDataSource) : HomeRepo {

    override suspend fun getProductList()
        = Pager(
                config = PagingConfig(pageSize = NETWORK_PAGE_SIZE,prefetchDistance = 2, initialLoadSize = 1),
                pagingSourceFactory = {
                    HomePagingSource(dataSource)
                }
            ).flow


    companion object {
        private const val NETWORK_PAGE_SIZE = 20
    }
}