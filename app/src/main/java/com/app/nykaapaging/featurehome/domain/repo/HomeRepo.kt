package com.app.nykaapaging.featurehome.domain.repo

import androidx.paging.PagingData
import com.app.nykaapaging.featurehome.data.model.ProductList
import kotlinx.coroutines.flow.Flow

interface HomeRepo {
    suspend fun getProductList() : Flow<PagingData<ProductList.Response.Product>>

}