package com.app.nykaapaging.featurehome.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.app.nykaapaging.featurehome.data.model.ProductList
import com.app.nykaapaging.featurehome.domain.usecase.HomeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val useCase: HomeUseCase) : ViewModel() {

    private val _productData = MutableStateFlow<PagingData<ProductList.Response.Product>?>(null)
    val productData = _productData.asStateFlow()

    init {
        getProductList()
    }

    private fun getProductList() {
        viewModelScope.launch {
            useCase.getProductList().cachedIn(viewModelScope).collectLatest {
                _productData.value = it
            }
        }
    }
}