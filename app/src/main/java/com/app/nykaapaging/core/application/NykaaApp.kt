package com.app.nykaapaging.core.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NykaaApp : Application()