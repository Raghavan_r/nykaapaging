package com.app.nykaapaging.di

import com.app.nykaapaging.featurehome.data.datasource.HomeDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {

    @Provides
    fun provideSampleDataSource(retrofit: Retrofit): HomeDataSource {
        return retrofit.create(HomeDataSource::class.java)
    }

}