package com.app.nykaapaging.di

import com.app.nykaapaging.featurehome.data.repo.HomeRepoImpl
import com.app.nykaapaging.featurehome.domain.repo.HomeRepo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepoModule {

    @Singleton
    @Binds
    abstract fun provideHomeRepo(homeRepo: HomeRepoImpl): HomeRepo

}