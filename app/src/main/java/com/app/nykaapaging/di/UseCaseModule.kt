package com.app.nykaapaging.di

import com.app.nykaapaging.featurehome.domain.repo.HomeRepo
import com.app.nykaapaging.featurehome.domain.usecase.HomeUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {

    @Provides
    @ViewModelScoped
    fun provideHomeUseCase(homeRepo : HomeRepo) = HomeUseCase(homeRepo)

}